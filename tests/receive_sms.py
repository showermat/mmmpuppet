#!/usr/bin/python3
from gi.repository import GLib
import dbus
import dbus.mainloop.glib
from enum import Enum

class MMSmsState(Enum):
  #https://www.freedesktop.org/software/ModemManager/doc/latest/ModemManager/ModemManager-Flags-and-Enumerations.html#MMSmsState
  MM_SMS_STATE_UNKNOWN   = 0
  MM_SMS_STATE_STORED    = 1
  MM_SMS_STATE_RECEIVING = 2
  MM_SMS_STATE_RECEIVED  = 3
  MM_SMS_STATE_SENDING   = 4
  MM_SMS_STATE_SENT      = 5

class MMSmsPduType(Enum):
  #https://www.freedesktop.org/software/ModemManager/doc/latest/ModemManager/ModemManager-Flags-and-Enumerations.html#MMSmsPduType
  MM_SMS_PDU_TYPE_UNKNOWN                       = 0
  MM_SMS_PDU_TYPE_DELIVER                       = 1
  MM_SMS_PDU_TYPE_SUBMIT                        = 2
  MM_SMS_PDU_TYPE_STATUS_REPORT                 = 3
  MM_SMS_PDU_TYPE_CDMA_DELIVER                  = 4
  MM_SMS_PDU_TYPE_CDMA_SUBMIT                   = 5
  MM_SMS_PDU_TYPE_CDMA_CANCELLATION             = 6
  MM_SMS_PDU_TYPE_CDMA_DELIVERY_ACKNOWLEDGEMENT = 7
  MM_SMS_PDU_TYPE_CDMA_USER_ACKNOWLEDGEMENT     = 8
  MM_SMS_PDU_TYPE_CDMA_READ_ACKNOWLEDGEMENT     = 9

class MMSmsValidityType(Enum):
  MM_SMS_VALIDITY_TYPE_UNKNOWN  = 0
  MM_SMS_VALIDITY_TYPE_RELATIVE = 1
  MM_SMS_VALIDITY_TYPE_ABSOLUTE = 2
  MM_SMS_VALIDITY_TYPE_ENHANCED = 3

def get_active_modem():
  """Goal: Find and return the first active modem DBUS path.
  IE: /org/freedesktop/ModemManager1/Modem/0

  I've noticed the modem will change if the phone goes to sleep so this
  will need to be called before interacting with the modem each time.
  There's probably a better way to do this but I cant figure it out.

  Success - return String of modem
  Fail    - return None
  """ #TODO - I bet there's a better way to do this.

  modem_objects = None
  active_modem = None

  #build the dbus connection to search for modem
  system_bus = dbus.SystemBus()
  mm_modem = system_bus.get_object('org.freedesktop.ModemManager1', '/org/freedesktop/ModemManager1')
  bus_iface = dbus.Interface(mm_modem, dbus_interface='org.freedesktop.DBus.ObjectManager')

  #Try and actually Search for the modem path on the dbus.
  try:
    modem_objects = bus_iface.GetManagedObjects('/org/freedesktop/ModemManager1/Modem')

  except Exception as problem:
    print(f"Problem looking for modem: {problem} ")


  #pull out the dbus path of the modem, If we have one.
  if(modem_objects != None):
    for modem in modem_objects:
      active_modem = modem
      break

  return active_modem

def get_sms_data_from_mm(dbus_sms_url):
  """
  """
  sms_byte_data = []

  #build up the DBUS interface
  system_bus = dbus.SystemBus()
  dbus_sms_object = system_bus.get_object('org.freedesktop.ModemManager1', dbus_sms_url)
  dbus_sms_properties = dbus.Interface(dbus_sms_object, dbus_interface='org.freedesktop.DBus.Properties')

  #Get everything about the SMS message.
  sms_properties = dbus_sms_properties.GetAll("org.freedesktop.ModemManager1.Sms")

  #Add a case for each type, We might want to do something with each.
  for key in sms_properties:
    if( key == "State" ):
      print(f"Key: {key} - Value {MMSmsState( sms_properties[key] ).name}")
    elif( key == "Number" ):
      print(f"Key: {key} - Value {sms_properties[key]}")
    elif( key == "PduType" ):
      print(f"Key: {key} - Value {MMSmsPduType( sms_properties[key] ).name}")
    elif( key == "Text" ):
      print(f"Key: {key} - Value {sms_properties[key]}")
    elif( key == "Data" ):
      for byte in sms_properties[key]:
        sms_byte_data.append(chr(byte))
      print(f"Key: {key} - Value {sms_byte_data}")
    elif( key == "SMSC" ):
      print(f"Key: {key} - Value {sms_properties[key]}")
    elif( key == "Validity" ):
      print(f"Key: {key} - Value[0] {MMSmsValidityType( sms_properties[key][0] ).name}")
      print(f"Key: {key} - Value[1] {bool( sms_properties[key][1] )}")
    elif( key == "Class" ):
      print(f"Key: {key} - Value {sms_properties[key]}")
    elif( key == "TeleserviceId" ):
      print(f"Key: {key} - Value {sms_properties[key]}")
    elif( key == "ServiceCategory" ):
      print(f"Key: {key} - Value {sms_properties[key]}")
    elif( key == "DeliveryReportRequest" ):
      print(f"Key: {key} - Value {bool(sms_properties[key])}")
    elif( key == "MessageReference" ):
      print(f"Key: {key} - Value {sms_properties[key]}")
    elif( key == "Timestamp" ):
      print(f"Key: {key} - Value {sms_properties[key]}") 
    elif( key == "DischargeTimestamp" ):
      print(f"Key: {key} - Value {sms_properties[key]}")
    elif( key == "DeliveryState" ):
      print(f"Key: {key} - Value {sms_properties[key]}")
    elif( key == "Storage" ):
      print(f"Key: {key} - Value {sms_properties[key]}")
    else:
      print(f"!!Unexpected!! Key: {key} - Value {sms_properties[key]}")

  return 0

def message_added(dbus_sms_path, mm_received):
  
  #https://www.freedesktop.org/software/ModemManager/doc/latest/ModemManager/gdbus-org.freedesktop.ModemManager1.Modem.Messaging.html#gdbus-signal-org-freedesktop-ModemManager1-Modem-Messaging.Added
  if( not mm_received ):
    print(f"Outbound Message {dbus_sms_path} added to Modem.")
    get_sms_data_from_mm(dbus_sms_path)

  elif( mm_received ):
    print(f"Inbound Message {dbus_sms_path} added to Modem.")
    get_sms_data_from_mm(dbus_sms_path)


  else:
    print(f"You really should be here.")
    return None

  return 0


dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
mainloop=GLib.MainLoop()

#Get the active modem AFTER mainloop
mm_active_modem_dbus_path = get_active_modem()

#Start watching for new messages from Modem Manager.
system_bus = dbus.SystemBus()
system_bus.add_signal_receiver( handler_function = message_added,
                                bus_name="org.freedesktop.ModemManager1",
                                path = mm_active_modem_dbus_path,
                                dbus_interface = "org.freedesktop.ModemManager1.Modem.Messaging",
                                signal_name = "Added")
print(f"Watching for Messages \"Added\" to Modem Messaging.")


mainloop.run()


#main stuff
