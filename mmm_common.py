#!/usr/bin/python3

#Deal with phone numbers
import phonenumbers
from enum import Enum

from pathlib import Path

HOME = str(Path.home())
BASE_DIR = HOME + "/.config/mmm/"

#BASE_DIR      = "/tmp/mmmpuppet/"

CONFIG_FILE   = BASE_DIR + "conf.json"
LOG_FILE      = BASE_DIR + "mmmpuppet.log"
DATABASE_FILE = BASE_DIR + "mmmpuppet.sqlite3"

default_country = "CA"
my_formatted_cell_number = 0

max_mms_size = 1000000

matrix_users_list = ""

matrix_client = None

matrix_event_loop = None

main_config = None


class MMSmsState(Enum):
  #https://www.freedesktop.org/software/ModemManager/doc/latest/ModemManager/ModemManager-Flags-and-Enumerations.html#MMSmsState
  MM_SMS_STATE_UNKNOWN   = 0
  MM_SMS_STATE_STORED    = 1
  MM_SMS_STATE_RECEIVING = 2
  MM_SMS_STATE_RECEIVED  = 3
  MM_SMS_STATE_SENDING   = 4
  MM_SMS_STATE_SENT      = 5

class MMSmsCdmaServiceCategory(Enum):
  #https://www.freedesktop.org/software/ModemManager/doc/latest/ModemManager/ModemManager-Flags-and-Enumerations.html#MMSmsCdmaServiceCategory
  MM_SMS_CDMA_SERVICE_CATEGORY_UNKNOWN = 0

class MMSmsDeliveryState(Enum):
  #https://www.freedesktop.org/software/ModemManager/doc/latest/ModemManager/ModemManager-Flags-and-Enumerations.html#MMSmsDeliveryState
  MM_SMS_DELIVERY_STATE_COMPLETED_RECEIVED = 0

message_pack = { "rooms_id": 0,
                 "state": MMSmsState.MM_SMS_STATE_UNKNOWN.value,
                 "number": "",
                 "mmsgroup": "",
                 "text": "",
                 "data": "",
                 "service_category": MMSmsCdmaServiceCategory.MM_SMS_CDMA_SERVICE_CATEGORY_UNKNOWN.value,
                 "delivery_report_request": True,
                 "timestamp": "",
                 "delivery_state": 0, #MMSmsDeliveryState
                 "content_type": "", 
                 "size": 0,
                 "bridged": False,
                 "bridged_attempt": 0,
                 "event": ""}



def format_number(number):
  #mmm_log.info("mmm_common.format_number", "Called.")
  #mmm_log.debug("mmm_common.format_number", "pre: " + str(number))

  to_format=phonenumbers.parse(number, default_country)
  formatted_number=phonenumbers.format_number(to_format, phonenumbers.PhoneNumberFormat.E164)

  #mmm_log.debug("mmm_common.format_number", "post: " + str(formatted_number))

  return formatted_number

def test_number(number):
  #mmm_log.info("mmm_common.test_number", "Called.")
  tested_number = False

  try:
    formatted=phonenumbers.parse(number, default_country) 
    tested_number=phonenumbers.is_possible_number(formatted)

  except Exception as problem:
    tested_number = False

  return tested_number
