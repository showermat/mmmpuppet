#!/usr/bin/python3

import mmm_common
import mmm_log
import mmm_database
import mmm_matrix

from gi.repository import GLib
import dbus
import dbus.mainloop.glib
import asyncio
import time
import random

from pathlib import Path


#Import copy to make a copy of the MMS Attachment dict.
#This is because if there's more than one MMS attachment the second
#is already editing the dict before the first is sent.
import copy

def message_added(dbus_mms_path, mms_data):
  """Goal: mmsd is pretty much the same as the mm sms messages. We'll
  have to talk to it over the dbus to let know it's read and delete the
  message from mmsd when its in the database.



  """#note to self - Only TODOs should be a problem.
  mmm_log.info("mmm_mmsd.message_added", "Called.")


  #if the message is not reclieved, We're sending (draft)
  if( mms_data['Status'] != "received" ):
    mmm_log.info("mmm_mmsd.message_added", "We're sending. Ignore.")
    return 0

  mmm_log.info("mmm_mmsd.message_added", "Incoming message.")


  #Grab a copy of the common message pack and update it with our unqie data
  message_pack = copy.deepcopy(mmm_common.message_pack)
  message_pack.update({"state": mmm_common.MMSmsState.MM_SMS_STATE_RECEIVED.value})
  message_pack.update({"number": mmm_common.format_number(str(mms_data['Sender']))})
  message_pack.update({"timestamp": str(mms_data['Date']) })

  mmsd_event = str(dbus_mms_path)
  message_pack.update({"event": mmsd_event})
  


  #Pull out a formatted list of recipients.
  mms_data_recipients = []
  for number in mms_data['Recipients']:
    mms_data_recipients.append(mmm_common.format_number(number))

  #Remove our number from the list of Recipients, We don't want it.
  try:
    mms_data_recipients.remove(mmm_common.my_formatted_cell_number)
  except Exception as problem:
    mmm_log.warning("mmm_mmsd.message_added", "did you set the right phone number in the conf? How did we recieve an MMS if we're not in the recipients list?")
    mmm_log.debug("mmm_mmsd.message_added", "Your number " + str(mmm_common.my_formatted_cell_number) + " should be in this list " + str(mms_data_recipients) )
    return None

  #We've removed our number so if there is at least 1 in the list then
  # its a group MMS and we need to create a local group ID.
  if( len(mms_data_recipients) > 0 ):
    mmm_log.info("mmm_mmsd.message_added", "Group MMS. Looking for group in DB.")

    #excluding us, Get a list of everyone (sender and recipients)
    mms_group_list = copy.deepcopy(mms_data_recipients)
    mms_group_list.append(message_pack['number'])

    #Use that list to check for a local Group ID.
    mms_group = mmm_database.find_mms_group(mms_group_list)

    if( mms_group == None ):
      mmm_log.critical("mmm_mmsd.message_added", "Error trying to find MMS group in database.")
      return None

    elif( mms_group == 0 ):
      mmm_log.info("mmm_mmsd.message_added", "didn't find group in db. Creating one")

      mms_group = mmm_database.create_mms_group(mms_group_list)
      if( mms_group == None ):
        mmm_log.critical("mmm_mmsd.message_added", "Problem with Group memebers Database. Leave message alone and retry later.")
        return None
      else:
        mmm_log.debug("mmm_mmsd.message_added", "New Group in database: "+ str(mms_group))
        message_pack.update({"mmsgroup": mms_group})
      
    else:
      mmm_log.debug("mmm_mmsd.message_added", "Found Group in database: " + mms_group)
      message_pack.update({"mmsgroup": mms_group})
      
  else:
    mmm_log.info("mmm_mmsd.message_added", "1:1 MMS message. No group stuff to do.")

  #Now that we know if its a group or not, Look for a local Room ID.
  mmm_log.info("mmm_mm.message_added", "Looking for local room.")
  if( len(mms_data_recipients) > 0 ):
    db_rooms_id = mmm_database.find_local_room_id(member=message_pack['mmsgroup'])
  else:
    db_rooms_id = mmm_database.find_local_room_id(member=message_pack['number'])

  
  if( db_rooms_id == None ):
    mmm_log.error("mmm_mm.message_added", "Database failed looking for local room")
    return None

  elif( db_rooms_id == 0 ):
    mmm_log.info("mmm_mm.message_added", "No local room, creating one.")
    if( len(mms_data_recipients) > 0 ):
      db_rooms_id = mmm_database.create_local_room_id(member=message_pack['mmsgroup'])
    else:
      db_rooms_id = mmm_database.create_local_room_id(member=message_pack['number'])

    if( db_rooms_id == None ):
      mmm_log.error("mmm_mm.message_added", "Database failed creating local room")
      return None


  mmm_log.debug("mmm_mm.message_added", "Local room: " + str(db_rooms_id) )
  message_pack.update({"rooms_id":db_rooms_id})

  mmm_log.debug("mmm_mmsd.message_added", "pack before attachments:" + str(message_pack))


  #Make a new message pack for each attachment and append it to a list
  # of message packs to be saved and bridged.
  list_of_message_packs = []

  for attachment in mms_data['Attachments']:
    mmm_log.info("mmm_mmsd.message_added", "Found an Attachment.")

    #Clean out text and data for new message pack
    message_pack.update({"text": ""})
    message_pack.update({"data": ""})

    message_pack.update({"content_type": str(attachment[1])})
    message_pack.update({"size": int(attachment[4])})

    mmm_log.info("mmm_mmsd.message_added", "discarding " + str(attachment[3]) + " keeping " + str(attachment[4]))
    in_file = open(attachment[2], "rb")
    header  = in_file.read(attachment[3]) #Smil
    header  = None #get rid of it
    mms_data    = in_file.read(attachment[4])
    in_file.close()

    if( attachment[1] == "text/plain;charset=utf-8" ):
      mmm_log.info("mmm_mmsd.message_added", "Text \"attachment\", Don't bother putting it into a file.")
      message_pack.update({"text": mms_data.decode("utf-8")})
      message_pack.update({"content_type": "text/plain"})

    else:
      mmm_log.info("mmm_mmsd.message_added", "real attachment. Write it to a file to bridge.")
      attachment_file_to_bridge = "/tmp/"+attachment[2].split("/",6)[5]+"-"+message_pack['number']+"-"+str(attachment[4])+"."+attachment[1].split("/",2)[1]
      opened_attachment_file = open(attachment_file_to_bridge, "wb")
      opened_attachment_file.write(mms_data)
      opened_attachment_file.close()
      message_pack.update({"data": attachment_file_to_bridge})


    mmm_log.debug("mmm_mmsd.message_added", "New Message Pack With an attachment: " + str(message_pack))
    list_of_message_packs.append(copy.deepcopy(message_pack))

  mmm_log.debug("mmm_mmsd.message_added", "List of all messages Pack " + str(list_of_message_packs))

  
  write_to_db_failed = False
  for message_pack in list_of_message_packs:
    """Only Delete the message from MMSD if all messages are written to
    the Database. If we only get part way though this, I would rather
    Have that data duplicated to Matirx then Delete the message off mmsd
    and only get part of the message.
    """
    mmm_log.info("mmm_mmsd.message_added", "Writing a message to the database.")

    db_message_id = mmm_database.write_new_message(message_pack)
    if( db_message_id != None and db_message_id > 0 ):
      mmm_log.debug("mmm_mmsd.message_added", "Message written to db ID: " + str(db_message_id))
      message_pack.update({"message_id": db_message_id})
    else:
      mmm_log.debug("mmm_mmsd.message_added", "failed to write "+ str(message_pack) + " to the Database")
      write_to_db_failed = True


  if(write_to_db_failed):
    mmm_log.error("mmm_mmsd.message_added", "Failed to write all Attachments to Database. You might see duplicate messages in Matix when we retry failed messages.")
    return None

  
  mmm_log.info("mmm_mmsd.message_added", "At this point, All mms attachments are in the Database. Going to deal with MMSD.")
  mmm_log.debug("mmm_mmsd.message_added", "MMSD - " + mmsd_event )

  
  mark_read_mms_return = mark_read_mms(dbus_mms_path)
  if( mark_read_mms_return == None ):
    mmm_log.warning("mmm_mmsd.message_added", "Failed to mark MMS as read")
  else:
    mmm_log.info("mmm_mmsd.message_added", "MMS marked as read")


  delete_mms_return = delete_mms(mmsd_event)
  if( delete_mms_return == None ):
    mmm_log.warning("mmm_mmsd.message_added", "delete from mmsd failed. We will likely get duplicate messages because of this.")
  else:
    mmm_log.info("mmm_mmsd.message_added", "delete from mmsd successful")



  #Go though and tell matrix about each message...slowly...
  for message_pack in list_of_message_packs:
    #loop = asyncio.new_event_loop()     #Matrix ID
    #loop = mmm_common.matrix_event_loop #Event loop already running
    #asyncio.set_event_loop(loop)
    #result = loop.run_until_complete( mmm_matrix.bridge_message_to_room(copy.deepcopy(message_pack)) )

    asyncio.run_coroutine_threadsafe(mmm_matrix.bridge_message_to_room(copy.deepcopy(message_pack)), mmm_common.matrix_event_loop)
    """AHHHHH TODO, yet another sleep. this for loop will blast though
    so quickly that if we need to make a matrix room each message will
    get its own room.

    To recreate the issue: Remove sleep below. Send an MMS with several
    attachments from a number without a room. The script will make
    several rooms.
    """
    time.sleep(5)



  mmm_log.info("mmm_mmsd.message_added", "All done here. Anything that failed to be bridged will be retried elsewhere.")

  return 0

def send_mms(numbers_list, text_string, file_path_string, file_type):
  """List of numbers, One or more.
  Either a string of text or a path to a local file. Both is fine.

  return None - failure
  return 0 - fine
  """
  mmm_log.info("mmm_mmsd.send_mms", "Called.")

  if(text_string == None and file_path_string == None):
    mmm_log.error("mmm_mmsd.send_mms", "cant find text or file")
    return None

  if (not isinstance(numbers_list, list) ):
    mmm_log.error("mmm_mmsd.send_mms", "Not a list.")
    return None
  elif( len(numbers_list) < 1 ):
    mmm_log.error("mmm_mmsd.send_mms", "Need a list of at least one number.")
    return None

  #Get the mmsd dbus information and build an object to send to.
  session_bus = dbus.SessionBus()

  try:
    manager = dbus.Interface(session_bus.get_object('org.ofono.mms', '/org/ofono/mms'), 'org.ofono.mms.Manager')
  except Exception as problem:
    mmm_log.error("mmm_mmsd.send_mms", "cant find MMSD. Is it running?")
    return None

  services = manager.GetServices()
  path = services[0][0]
  service = dbus.Interface(session_bus.get_object('org.ofono.mms', path),'org.ofono.mms.Service')


  #Build a recipients dbus array from list of numbers.
  recipients = dbus.Array([],signature=dbus.Signature('s'))
  for number in numbers_list:
    recipients.append(dbus.String(number))

  mmm_log.info("mmm_mmsd.send_mms", "number list " + text_string )

  #let mmsd build the smil, The example I have dones this too.
  #python3 send-message "+15556661234,+15556661234" "" "cid-1,text/plain,/home/untidylamp/send/text.txt" "cid-2,image/jpeg,/home/untidylamp/send/image.jpg"
  smil = ""

  #Here we build the attachments.
  attachments = dbus.Array([],signature=dbus.Signature('(sss)'))

  #declare it here because we test .is_file at the end to delete.
  tmp_txt_file = "/tmp/"+str(int(time.time()))+"-"+str(random.randrange(10, 99))+".txt"
  
  if(text_string != None and text_string != ""):
    #We take the text and write to /tmp/, pass that file to SendMessage
    #Then delete it.
    mmm_log.info("mmm_mmsd.send_mms", "text to write to file and send: " + text_string )
    
    
    with open(tmp_txt_file, "w") as open_text_file:
      open_text_file.write(text_string)

    attachments.append(dbus.Struct((dbus.String("txt_file_name"),
                       dbus.String("text/plain"),
                       dbus.String(tmp_txt_file)
                      ), signature=None))

  #Not elif, we can attach both.
  if(file_path_string != None and file_path_string != "" ):
    mmm_log.info("mmm_mmsd.send_mms", "file to attach: " + str(file_path_string) )
    attachments.append(dbus.Struct((dbus.String("file_name"),
                       dbus.String(file_type),
                       dbus.String(file_path_string)
                      ), signature=None))



  #Send the MMS
  mmm_log.debug("mmm_mmsd.send_mms", "mms to be sent " + str(recipients) + " " + str(smil) + " " + str(attachments) )

  try:
    mms_send_message_return = service.SendMessage(recipients, smil, attachments)
    mms_send_message_return = str(mms_send_message_return)
  except Exception as problem:
    mmm_log.error("mmm_mmsd.send_mms", "Problem sending mms to mmsd.")
    mmm_log.debug("mmm_mmsd.send_mms", "Exception " + str(problem))
    mms_send_message_return = None

  #clean up temp text file. Dont touch the other file, We didn't make it
  if Path(tmp_txt_file).is_file():
    mmm_log.info("mmm_mmsd.send_mms", "deleting tmp text file" )
    Path(tmp_txt_file).unlink()



  return mms_send_message_return

def notify_sms_wap(list_of_bytes_from_mm_data):
  """Goal, take the DATA part of an SMS on the modem and send it to
   MMSD for download.

  It's a safe assumption that if we do not receive an error, MMSD has
   Our data and we can delete it from the Modem.

  If MMSD has a problem with it in any way we'll have to query MMSD
   directly for retry, status, etc.

  return 0 if No errors (success, can delete from modem.)
  return None if errors (dont delete off modem)
  """#Notes for me: We're going to trust this.
  mmm_log.info("mmm_mmsd.notify_sms_wap", "Called")
  dbus_byte_array = dbus.Array([])

  
  for byte in list_of_bytes_from_mm_data:
    dbus_byte_array.append(dbus.Byte(byte))

  session_bus = dbus.SessionBus()
  try:
    dbus_mmmms_object = session_bus.get_object('org.ofono.mms.ModemManager', '/org/ofono/mms')
  except Exception as problem:
    mmm_log.error("mmm_mmsd.notify_sms_wap", "Is mmsd running?")
    return None

  dbus_mmmms_properties = dbus.Interface(dbus_mmmms_object, dbus_interface='org.ofono.mms.ModemManager')

  try:
    dbus_mmmms_properties.PushNotify(dbus_byte_array)
  except Exception as problem:
    mmm_log.error("mmm_mmsd.notify_sms_wap", "problem with PushNotify to mmsd.")
    mmm_log.debug("mmm_mmsd.notify_sms_wap", "Exception: " + str(problem))

  mmm_log.info("mmm_mmsd.notify_sms_wap", "Messages sent over dbus to mmsd without error")
  return 0

def delete_mms(dbus_mms_path):
  """Goal: Delete an MMS message off mmsd.

  Success - return 0
  Fail    - return None
  """#Note for myself = Good
  mmm_log.info("mmm_mm.delete_mms", "Called.")
  delete_mms_return = None

  #Build up the messaging interface to delete an sms
  session_bus = dbus.SessionBus()

  dbus_modem_object = session_bus.get_object('org.ofono.mms', dbus_mms_path)
  dbus_messaging_interface = dbus.Interface(dbus_modem_object, dbus_interface='org.ofono.mms.Message')

  try:
    #using the path delete the sms.
    delete_mms_return = dbus_messaging_interface.Delete() #always returns None. If it doesn't error then it was successful.
    delete_mms_return = 0

  except Exception as problem:
    mmm_log.critical("mmm_mm.delete_mms", "Cant delete message off modem")
    mmm_log.debug("mmm_mm.delete_mms", "Exception: " + str(problem))

  return delete_mms_return

def mark_read_mms(dbus_mms_path):
  """Goal: mark an incoming MMS as read in mmsd.

  Success - return 0
  Fail    - return None
  """#Note for myself = Good
  mmm_log.info("mmm_mm.mark_read_mms", "Called.")
  mark_read_mms_return = None

  #Build up the messaging interface to delete an sms
  session_bus = dbus.SessionBus()

  dbus_modem_object = session_bus.get_object('org.ofono.mms', dbus_mms_path)
  dbus_messaging_interface = dbus.Interface(dbus_modem_object, dbus_interface='org.ofono.mms.Message')

  try:
    #using the path delete the sms.
    mark_read_mms_return = dbus_messaging_interface.MarkRead() #always returns None. If it doesn't error then it was successful.
    mark_read_mms_return = 0

  except Exception as problem:
    mmm_log.critical("mmm_mm.mark_read_mms", "Cant delete message off modem")
    mmm_log.debug("mmm_mm.mark_read_mms", "Exception: " + str(problem))

  return mark_read_mms_return
