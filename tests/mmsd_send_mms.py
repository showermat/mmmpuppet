#!/usr/bin/python3

import dbus
from pathlib import Path

numbers_list = ["+12505551234", "+12145554321"]
text_string  = "this is an MMS message"
#file_path_string = "/path/to/pic"
file_path_string = ""

if( len(numbers_list) >= 1 ):

  #Get the mmsd dbus information and build an object to send to.
  bus = dbus.SessionBus()
  manager = dbus.Interface(bus.get_object('org.ofono.mms', '/org/ofono/mms'), 'org.ofono.mms.Manager')
  services = manager.GetServices()
  path = services[0][0]
  service = dbus.Interface(bus.get_object('org.ofono.mms', path),'org.ofono.mms.Service')


  #Build a recipients dbus array from list of numbers.
  recipients = dbus.Array([],signature=dbus.Signature('s'))
  for number in numbers_list:
    recipients.append(dbus.String(number))


  #let mmsd build the smil, The example I have dones this too.
  #python3 send-message "+15556661234,+15556661234" "" "cid-1,text/plain,/home/untidylamp/send/text.txt" "cid-2,image/jpeg,/home/untidylamp/send/image.jpg"
  smil = ""

  #Here we build the attachments.
  attachments = dbus.Array([],signature=dbus.Signature('(sss)'))

  #declare it here because we test .is_file at the end to delete.
  tmp_txt_file = "/tmp/mmsd_send_mms.txt"
  
  if(text_string != None and text_string != ""):
    #MMSD requres files for all attachments, even text. Create a tmp one.
    with open(tmp_txt_file, "w") as open_text_file:
      open_text_file.write(text_string)

    attachments.append(dbus.Struct((dbus.String("txt_file_name"),
              dbus.String("text/plain"),
              dbus.String(tmp_txt_file)
              ), signature=None))

  #Not elif, we can attach both.
  if(file_path_string != None and file_path_string != "" ):
    attachments.append(dbus.Struct((dbus.String("file_name"),
              dbus.String("image/jpeg"),
              dbus.String(file_path_string)
              ), signature=None))



  #Send the MMS
  print(f"MMS to be sent. recipients {str(recipients)} smil {str(smil)} attachments {str(attachments)} ")
  send_mms_return = service.SendMessage(recipients, smil, attachments)


  #clean up temp text file. Dont touch the other file, We didn't make it
  if Path(tmp_txt_file).is_file():
    print(f"Deleting tmp text file")
    Path(tmp_txt_file).unlink()

  print(f"Sent MMS {send_mms_return}")

else:
  print(f"Is it a number list of at least one?")


