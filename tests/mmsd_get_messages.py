#!/usr/bin/python3

import dbus
from pathlib import Path

#Get the mmsd dbus information and build an object to send to.
bus = dbus.SessionBus()
manager = dbus.Interface(bus.get_object('org.ofono.mms', '/org/ofono/mms'), 'org.ofono.mms.Manager')
services = manager.GetServices()
path = services[0][0]
print(str(path))
service = dbus.Interface(bus.get_object('org.ofono.mms', path),'org.ofono.mms.Service')
send_mms_return = service.GetMessages( )

print(str(send_mms_return))


  
