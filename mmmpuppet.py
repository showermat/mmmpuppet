#!/usr/bin/python3


from pathlib import Path
import asyncio

from gi.repository import GLib
import dbus
import dbus.mainloop.glib

import threading


#my files
import mmm_log
import mmm_config
import mmm_matrix
import mmm_mm
import mmm_mmsd
import mmm_database
import mmm_common

from nio import (
    SyncResponse,
    RoomMessageText,
    RoomMessageFile,
    RoomMessageImage,
    InviteEvent,
    RoomNameEvent,
    RoomMessageVideo,
    RoomMessageAudio,
    RoomMemberEvent,
)

def dbus_thread_main():
  mmm_log.info("dbus_thread_main", "Starting")

  dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
  mainloop=GLib.MainLoop()

  #Get the active modem AFTER mainloop
  active_modem = mmm_mm.get_active_modem()

  #Start watching for new messages from Modem Manager.
  system_bus = dbus.SystemBus()
  system_bus.add_signal_receiver( handler_function = mmm_mm.message_added,
                                  bus_name="org.freedesktop.ModemManager1",
                                  path = active_modem,
                                  dbus_interface = "org.freedesktop.ModemManager1.Modem.Messaging",
                                  signal_name = "Added")
  mmm_log.info("dbus_thread_main", "Watching for Modem Messages.")


  #Start watching for new messages from mmsd
  session_bus = dbus.SessionBus()
  session_bus.add_signal_receiver( handler_function = mmm_mmsd.message_added,
                                   bus_name="org.ofono.mms",
                                   path = "/org/ofono/mms/modemmanager",
                                   dbus_interface = "org.ofono.mms.Service",
                                   signal_name = "MessageAdded")
  mmm_log.info("dbus_thread_main", "Watching for replies from mmsd.")


  mainloop.run()
  mmm_log.warning("dbus_thread_main", "Done.")
  return 0

def matrix_thread_main(main_config):
  mmm_log.info("matrix_thread_main", "Starting.")

  mmm_common.matrix_event_loop = asyncio.new_event_loop()

  mmm_common.matrix_event_loop.run_until_complete(matrix_client_main(main_config))

  mmm_log.warning("matrix_thread_main", "Done.")
  return 0

async def matrix_client_main(main_config):
  mmm_log.info("matrix_client_main", "Starting")

  #Restore old session.
  matrix_client = await mmm_matrix.login_with_token(main_config)

  if( matrix_client != None ):
    mmm_log.info("matrix_client_main", "Restore successful....MAYBE...Test this somehow??? TODO)")


    #stuff the client into mmm_common so we can call it from the dbus thread
    mmm_common.matrix_client = matrix_client

    #Add sync response. This'll write the sync time directly to the Database.
    matrix_client.add_response_callback(mmm_matrix.sync_callback, SyncResponse)


    #Check for a sync_token in Database. If we dont have one sync once
    # to discard all old/stale messages so we dont spam cell numbers.
    since_token = mmm_database.get_token()
    if( since_token == None or since_token == "fresh_start" ):
      mmm_log.warning("matrix_client_main", "No sync_token in Database. Discarding old messages to start fresh.")
      since_token=""
      await mmm_common.matrix_client.sync(timeout=500, set_presence="online")


    mmm_common.matrix_client.add_event_callback(mmm_matrix.m_room_message_callback, RoomMessageText)
    mmm_common.matrix_client.add_event_callback(mmm_matrix.m_room_message_callback, RoomMessageImage)
    mmm_common.matrix_client.add_event_callback(mmm_matrix.m_room_message_callback, RoomMessageVideo)
    mmm_common.matrix_client.add_event_callback(mmm_matrix.m_room_message_callback, RoomMessageAudio)
    mmm_common.matrix_client.add_event_callback(mmm_matrix.m_room_message_callback, RoomMessageFile)
    mmm_common.matrix_client.add_event_callback(mmm_matrix.invite_callback, InviteEvent)
    #mmm_common.matrix_client.add_event_callback(mmm_matrix.test_callback, RoomNameEvent)
    mmm_common.matrix_client.add_event_callback(mmm_matrix.member_event_callback, RoomMemberEvent)

    
    
    await mmm_common.matrix_client.sync_forever(timeout=30000, set_presence="online", since=since_token)
    
  else:
    mmm_log.error("matrix_client_main", "Matrix restore failed. Check your token.")
    return None

  mmm_log.debug("matrix_client_main", "done" )
  return None

def main():
  #build up base path for everything this script does.
  Path(mmm_common.BASE_DIR).mkdir(parents=True, exist_ok=True)
  mmm_log.info("main", "Starting")

  #try to build the base DB. Exit if it fails.
  db_init_status = mmm_database.initialize()
  if( db_init_status == None ):
    mmm_log.critical("main", "database initialize failed.")
    print(f"Check the logs {mmm_common.LOG_FILE}")
    return None

  #read conf file
  main_config = mmm_config.read(mmm_common.CONFIG_FILE)

  #If we cant find the conf file, exit.
  if( main_config == None ):
    mmm_log.critical("main", "Please move your config to: " + mmm_common.CONFIG_FILE + " If its already there, check the permissions." )
    print(f"Check the logs {mmm_common.LOG_FILE}")
    return None

  #If we find a password, assume this is a fresh start and test login.
  elif( "bot_password" in main_config ):
    mmm_log.info("main", "Found password. Going to login and change to token.")
    login_info = asyncio.run(mmm_matrix.get_access_token_with_password(main_config))

    #if login was successful, remove password, add token and ID
    if( login_info != None ):
      main_config.pop("bot_password")
      main_config.update({"access_token": login_info["access_token"]})
      main_config.update({"device_id": login_info["device_id"]})

      #write to config
      write_return = mmm_config.write(mmm_common.CONFIG_FILE, main_config)

      #stop script if there's a problem writing to config
      if( write_return != None ):
        mmm_log.info("main", "Config updated with token.")
        print(f"Login successful. Config updated with token. Run again to start bridge.")
        
      else:
        mmm_log.critical("main", "Problem writting to config")
        return None

  #if we find a token then just login.
  elif( "access_token" in main_config ):
    mmm_log.info("main", "Found matrix token. Starting everything.")


    #set the default country in common. Default is Canada.
    mmm_common.default_country = main_config['cell_country']

    #grab my number from the config, format it, put it in common.
    mmm_common.my_formatted_cell_number = mmm_common.format_number(main_config['cell_number'])

    #get the list of users to invite to a new room.
    mmm_common.matrix_users_list = main_config['matrix_users_csv'].split(',')

    #Grab mms size limit
    mmm_common.max_mms_size = int(main_config['mms_size_limit'])

    thread_list = []

    #Thread for listening on the DBUS for Modem Mamager sms Messages.
    dbus_thread = threading.Thread(target=dbus_thread_main, args=(), name="dbus")
    dbus_thread.start()
    thread_list.append(dbus_thread)
    
    #Thread for listening on the DBUS for Modem Mamager sms Messages.
    #matrix_thread = threading.Thread(target=matrix_thread_main, args=(main_config,), name="matrix")
    #matrix_thread.start()
    #thread_list.append(matrix_thread) 
    mmm_common.matrix_event_loop = asyncio.get_event_loop()
    mmm_common.matrix_event_loop.run_until_complete(matrix_client_main(main_config))


    mmm_log.debug("main", "threads: " + str(thread_list) )


  else:
    mmm_log.critical("main", "Please double check your config file.")
    print(f"Check the logs {mmm_common.LOG_FILE}")
    return None

  mmm_log.debug("main", "done" )
  return None

if __name__ == "__main__":
  main()
