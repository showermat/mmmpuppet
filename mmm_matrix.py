#!/usr/bin/python3

import mmm_log
import mmm_database
import mmm_common
import mmm_mm
import mmm_mmsd

import aiofiles
import asyncio
from pathlib import Path

#to download matrix file
import urllib.request



from nio import (
    AsyncClient,
    Client,
    AsyncClientConfig,
    MatrixRoom,
    SyncResponse,
    RoomMessageText,
    RoomMessageFile,
    RoomMessageImage,
    RoomNameEvent,
    InviteEvent,
    UploadResponse,
    InviteEvent,
    RoomVisibility,
    LoginResponse,
    ToDeviceError,
    LocalProtocolError,
    JoinError,
    RoomMessageVideo,
    RoomMessageAudio,
    RoomMemberEvent,
)

#Import copy to make a copy of the message pack in mmm_common. Not sure
#if we NEED this or not.
import copy

async def mxc_to_url(mxc_url):
  """Goal: take a string that contains the matrix mxc url and ask matrix
  for the http URL.

  return None               - Failed
  return.startswith("http") - success
  """#Note to Self - good
  mmm_log.info("mmm_matrix.mxc_to_url", "called")

  http_download_url = None

  if( not mxc_url.startswith( "mxc:" ) ):
    mmm_log.error("mmm_matrix.mxc_to_url", "didn't get an mxc: url")
    return None

    
  try:
    http_download_url = await mmm_common.matrix_client.mxc_to_http(mxc_url)
  except Exception as problem:
    mmm_log.error("mmm_matrix.mxc_to_url", "Problem with mxc to url")
    mmm_log.debug("mmm_matrix.mxc_to_url", problem )


  if( http_download_url != None and http_download_url.startswith("http")):
    mmm_log.debug("mmm_matrix.mxc_to_url", "URL: " + http_download_url)
    return http_download_url

  mmm_log.error("mmm_matrix.mxc_to_url", "Failed to get URL")
  mmm_log.debug("mmm_matrix.mxc_to_url", "URL: " + str(http_download_url) )
  return None

async def download_http_file(http_url, file_name):
  """
  """

  mmm_log.info("mmm_matrix.download_http_file", "called")

  #build the tmp file path, make sure its empty.
  outfile = "/tmp/outbound-mms-"+file_name
  if Path(outfile).is_file():
    Path(outfile).unlink()

  #try and download the file to the temp path
  try:
    urllib.request.urlretrieve(http_url, outfile)
  except Exception as problem:
    mmm_log.error("mmm_matrix.download_http_file", "Download failed")
    mmm_log.debug("mmm_matrix.download_http_file", problem)
    return None

  mmm_log.info("mmm_matrix.download_http_file", "Downloaded")

  #if its there, return the path.
  if Path(outfile).is_file():
    mmm_log.debug("mmm_matrix.download_http_file", "path: " + outfile)
    return outfile
  else:
    return None

async def room_send_emote(matrix_room, message):
  """I use this to notify the user via matirx of actions the bot took.
  duplicate room,
  not a real phone number,
  etc.
  return event (starts with $) if success
  return None if failure
  """
  try:
    matrix_send_return = await mmm_common.matrix_client.room_send(
      room_id=matrix_room,
      message_type="m.room.message",
      content={
               "msgtype": "m.emote",
               "body": message
              })
  except Exception as problem:
    mmm_log.error("mmm_matrix.room_send_emote", "failed to send emote")
    mmm_log.debug("mmm_matrix.room_send_emote", "failed to send emote: " + message + " to room: " +  matrix_room + " problem: " + str(problem) )

  event = None
  try:
    event = str(matrix_send_return.event_id)
  except Exception as problem:
    mmm_log.debug("mmm_matrix.room_send_emote", "cant find emote event: " + str(problem) )
    mmm_log.debug("mmm_matrix.room_send_emote", "cant find emote event: " + str(problem) )

  return event

async def get_access_token_with_password(matirx_config):
  """All We're doing here is setting up our account.
  Login, Get a Token, return token.
  
  return None - Login failure
  return Token - Successful.
  """
  
  #If we haven't logged in before, login, pop password and add token to
  # the config file.
  mmm_log.info("mmm_matrix.login_with_password", "Trying to login to get access token.")
  mmm_log.debug("mmm_matrix.login_with_password", "homeserver: " + matirx_config['homeserver'])
  mmm_log.debug("mmm_matrix.login_with_password", "bot_account: " + matirx_config['bot_account'])
  mmm_log.debug("mmm_matrix.login_with_password", "bot_password: " + matirx_config['bot_password'])
  mmm_log.debug("mmm_matrix.login_with_password", "device_name: " + matirx_config['device_name'])

  matrix_client = AsyncClient(matirx_config['homeserver'],
                              matirx_config['bot_account'],)


  login_attempt = await matrix_client.login(matirx_config['bot_password'], device_name=matirx_config['device_name'])
  await matrix_client.close()

  #If the login attempt was successful, return dict of access token and device ID
  if (isinstance(login_attempt, LoginResponse)):
    mmm_log.debug("mmm_matrix.login_with_password", "access_token: " + login_attempt.access_token)
    mmm_log.debug("mmm_matrix.login_with_password", "device_id: " + login_attempt.device_id)
    return {"access_token": login_attempt.access_token, "device_id": login_attempt.device_id}

  #if login failed, print message to logs.
  else:
    mmm_log.critical("mmm_matrix.login_with_password", "Error trying to login." + str(login_attempt))
    return None

async def login_with_token(matirx_config):
  """login and return the matrix_client object if successful.
  
  return None          - Login failure
  return client object - Successful.
  """
  
  #If we haven't logged in before, login, pop password and add token to
  # the config file.
  mmm_log.info("mmm_matrix.login_with_token", "Trying to login to get access token.")

  matrix_client = AsyncClient( homeserver = matirx_config['homeserver'],
                               user       = matirx_config['bot_account'],
                               device_id  = matirx_config['device_id'])

  matrix_client.restore_login( user_id      = matirx_config['bot_account'],
                               device_id    = matirx_config['device_id'],
                               access_token = matirx_config['access_token'])




  #TODO, test this somehow?!
  return matrix_client

  #https://github.com/poljar/matrix-nio/blob/master/nio/client/async_client.py#L312
  print(type(matrix_client))
  if (matrix_client):
    mmm_log.debug("mmm_matrix.login_with_token", "good. ")
    return matrix_client

  #if login failed, print message to logs.
  else:
    mmm_log.critical("mmm_matrix.login_with_token", "Error with Token." + str(matrix_client))
    return None

async def sync_callback(response):
  """This'll be called every X seconds by the matrix_client.
  it will write the last sync time directly to the database so if it
  crashes or we restart the program we can read from the database
  and start off at the same point in time.

  Returns None

  Will log errors though.
  """
  mmm_log.info("mmm_matrix.sync_callback", "Called.")
  mmm_log.debug("mmm_matrix.sync_callback", "since_token: " + response.next_batch)
  mmm_database.write_sync_callback(response.next_batch)
  return 0

async def test_callback(room: MatrixRoom, event: RoomMessageFile) -> None:
  """Just testing callback events.
  """
  mmm_log.info("mmm_matrix.test_callback", "Called.")
  mmm_log.debug("mmm_matrix.test_callback", str(room))
  mmm_log.debug("mmm_matrix.test_callback", str(event))
  mmm_log.info("mmm_matrix.test_callback", "done.")

async def member_event_callback(room: MatrixRoom, event: RoomMessageFile) -> None:
  """Just testing callback events.
  """
  mmm_log.info("mmm_matrix.member_event_callback", "Called.")


  event_type = ""
  try:
    event_type = str(event.membership)
  except Exception as problem:
    mmm_log.debug("mmm_matrix.member_event_callback", "Event type empty." )
    return 0

  if( event_type == "leave" ):
    mmm_log.debug("mmm_matrix.member_event_callback", event_type + " from: " + str(event.sender) + " for room: " + str(room.room_id) )

    if( event.sender in mmm_common.matrix_users_list ):
      mmm_log.info("mmm_matrix.member_event_callback", "Trusted user is leaving. I'll leave too." )

      await mmm_common.matrix_client.room_leave(room.room_id)
      await mmm_common.matrix_client.room_forget(room.room_id)

      deleted_room_id = mmm_database.delete_room(room.room_id)
      mmm_log.debug("mmm_matrix.member_event_callback", "deleted room id: " + str(deleted_room_id) )

      if( deleted_room_id == None ):
        mmm_log.critical("mmm_matrix.member_event_callback", "Failed to Connect to Database" )

      elif( deleted_room_id == 0 ):
        mmm_log.info("mmm_matrix.member_event_callback", "No room to clean up" )
        
      elif( deleted_room_id > 0 ):

        deleted_message_id = mmm_database.delete_room_messages(deleted_room_id)
        if( deleted_message_id == None ):
          mmm_log.error("mmm_matrix.member_event_callback", "Failed to clean up messages" )
        elif( deleted_message_id > 0 ):
          mmm_log.error("mmm_matrix.member_event_callback", "Message clean up successful" )
  else:
    mmm_log.debug("mmm_matrix.member_event_callback", "Ignoring " + event_type )
  return 0

async def invite_callback(room: MatrixRoom, event: RoomMessageFile) -> None:
  """This'll be to watch for matrix room invites. Make sure the sender
  is in the config file CSV, if so join the room, add to database, etc
  otherwise do nothing.

  Return None on failure
  return 0 on other
  """
  mmm_log.info("mmm_matrix.invite_callback", "Called.")


  event_type = ""
  try:
    event_type = str(event.membership)
  except Exception as problem:
    mmm_log.debug("mmm_matrix.invite_callback", "Not an event we care about." )
    return 0


  if( event_type == "invite" ):
    mmm_log.debug("mmm_matrix.invite_callback", "Invite from: " + str(event.sender) + " for room: " + room.room_id )

    if( event.sender in mmm_common.matrix_users_list ):
      mmm_log.info("mmm_matrix.invite_callback", "Invite from trusted user." )

      join_response = await mmm_common.matrix_client.join(room.room_id)

      if isinstance(join_response, JoinError):
        mmm_log.error("mmm_matrix.invite_callback", "Trying to join room failed." )
        mmm_log.debug("mmm_matrix.invite_callback", "join failed: " + str(join_response) )
        return None

      room_name = await get_room_name(room.room_id)
      room_topic = await get_room_topic(room.room_id)

      mmm_log.debug("mmm_matrix.invite_callback", "room_name " + str(room_name) + " room_topic " + str(room_topic) )

      if(room_name != None and room_topic != None):


        #split the numbers into a list and test them to see if they're real
        numbers_list = room_topic.split(',')

        formatted_number_list = []

        for number in numbers_list:
          is_topic_number = mmm_common.test_number(number)
          
          if( not is_topic_number ):
            await room_send_emote(room.room_id, "Problem with number: " + str(number) )
            await mmm_common.matrix_client.room_leave(room.room_id)
            await mmm_common.matrix_client.room_forget(room.room_id)
            return None
          else:
            formatted_number_list.append(mmm_common.format_number(number))

        #numbers are real, Check if its going to be a group or not,
        #then write to database.
        if( len(formatted_number_list) == 1 ):
          exsting_matrix_room = mmm_database.find_matrix_room_id(number=formatted_number_list[0])

          if( exsting_matrix_room == None ):
            mmm_log.error("mmm_matrix.invite_callback", "Problem with Database." )
            await room_send_emote(room.room_id, "Problem with Database. check the app logs.")
            await mmm_common.matrix_client.room_leave(room.room_id)
            await mmm_common.matrix_client.room_forget(room.room_id)
            return None

          elif( exsting_matrix_room == 0 ):
            mmm_log.warning("mmm_matrix.invite_callback", "Local room without Matirx ID. This is weird but we'll add our room to it." )
            await room_send_emote(room.room_id, "We already have a room.. Kind of.. An incoming message from this person must have caused a problem.")
            rooms_table_id = mmm_database.find_local_room_id(member=formatted_number_list[0])
            mmm_database.add_matirx_room_to_local_room(rooms_table_id, room.room_id)

          elif( exsting_matrix_room == 1 ):
            mmm_log.info("mmm_matrix.invite_callback", "No room, Building one." )
            await room_send_emote(room.room_id, "Building local room.")

            db_rooms_id = mmm_database.create_local_room_id(member=formatted_number_list[0], matrix_room_id=room.room_id)

            if( db_rooms_id == None ):
              mmm_log.warning("mmm_matrix.invite_callback", "Creating local room failed.")
              await room_send_emote(room.room_id, "Failed. Check script logs. " )
            else:
              mmm_log.info("mmm_matrix.invite_callback", "New room in DB." )
              mmm_log.debug("mmm_matrix.invite_callback", "Matrix room: " + room.room_id + " for " + formatted_number_list[0] + " in database." )
              await room_send_emote(room.room_id, "Start chatting with " + room_name )
              await room_send_emote(room.room_id, "NOTE: I will ignore encrypted messages. If this is an encrypted room I do nothing." )


          else:
            mmm_log.info("mmm_matrix.invite_callback", "already have a room for number we've been invited to" )
            mmm_log.debug("mmm_matrix.invite_callback", "New room ID: " + room.room_id + " is duplicate of: " + str(exsting_matrix_room) )
            await room_send_emote(room.room_id, "Already have a room for this: " + str(exsting_matrix_room) )
            await mmm_common.matrix_client.room_leave(room.room_id)
            await mmm_common.matrix_client.room_forget(room.room_id)
            return None




        #if there's more than one number, its a group.
        elif( len(formatted_number_list) > 1 ):

          #check if we already have an one already.
          mmsgroup = mmm_database.find_mms_group(formatted_number_list)

          if( mmsgroup == None ):
            mmm_log.error("mmm_matrix.invite_callback", "Problem with mmsgroup Database." )
            await room_send_emote(room.room_id, "Problem with Database. check the app logs.")
            await mmm_common.matrix_client.room_leave(room.room_id)
            await mmm_common.matrix_client.room_forget(room.room_id)
            return None
          elif( mmsgroup == 0 ):
            mmm_log.error("mmm_matrix.invite_callback", "Problem with mmsgroup Database." )
            await room_send_emote(room.room_id, "No Local mms room, Building one.")
          else:
            mmm_log.error("mmm_matrix.invite_callback", "Already have room for this" )
            exsting_matrix_room = mmm_database.find_matrix_room_id(group=mmsgroup)
            await room_send_emote(room.room_id, "Already have MMS group for this: " + str(exsting_matrix_room) )
            await mmm_common.matrix_client.room_leave(room.room_id)
            await mmm_common.matrix_client.room_forget(room.room_id)
            return None


          #We dont have a group like this yet, create one.          
          mmsgroup = mmm_database.create_mms_group(formatted_number_list)
          if( mmsgroup == None ):
            mmm_log.error("mmm_matrix.invite_callback", "Problem building new MMS group" )
            await room_send_emote(room.room_id, "Problem with Database. check the app logs.")
            await mmm_common.matrix_client.room_leave(room.room_id)
            await mmm_common.matrix_client.room_forget(room.room_id)
            return None
          elif( mmsgroup.startswith("group:") ):
            mmm_log.info("mmm_matrix.invite_callback", "We have group, build room.")
            db_rooms_id = mmm_database.create_local_room_id(member=mmsgroup, matrix_room_id=room.room_id)

            if( db_rooms_id == None ):
              #TODO remove mmsgroup from room. mmsgroup worked, room failed. BAD state.
              mmm_log.error("mmm_matrix.invite_callback", "Problem building local room with mmsgroup." )
              await room_send_emote(room.room_id, "Problem with Database. check the app logs.")
              await mmm_common.matrix_client.room_leave(room.room_id)
              await mmm_common.matrix_client.room_forget(room.room_id)
              return None
            else:
              mmm_log.debug("mmm_matrix.invite_callback", "Local room: " + str(db_rooms_id) )
              mmm_log.debug("mmm_matrix.invite_callback", "Matrix room: " + room.room_id + " for " + str(formatted_number_list) + " in database." )
              await room_send_emote(room.room_id, "Start chatting with group: " + room_name )

        
      else:
        mmm_log.info("mmm_matrix.invite_callback","Invited to a room with bad Topic or Name")
        await room_send_emote(room.room_id, "Name or Topic empty. Please update and reinvite me. ")
        await mmm_common.matrix_client.room_leave(room.room_id)
        await mmm_common.matrix_client.room_forget(room.room_id)
        return 0

    else:
      mmm_log.warning("mmm_matrix.invite_callback", "Ignoring Invite to new room. If this is a trusted user please add them to the config file list." )
      mmm_log.debug("mmm_matrix.invite_callback", "untrusted invite from: " + event.sender )


  return 0

async def m_room_message_callback(room: MatrixRoom, event: RoomMessageFile) -> None:
  """Take all the m.room.message posts to a room and write to database.

  Returns are not watched anywhere but the same rules are here.
  failure return None
  alright exit 0 
  """
  mmm_log.info("mmm_matrix.m_room_message_callback", "Called")

  matrix_event_type = ""

  if( room.own_user_id == event.sender ):
    mmm_log.info("mmm_matrix.m_room_message_callback", "Message from bot. Ignore.")
    return 0


  #Get the local Room ID
  rooms_db_id = mmm_database.find_local_room_id(matrix_room_id=room.room_id)

  if( rooms_db_id == None):
    mmm_log.critical("mmm_matrix.m_room_message_callback", "Problem with Database.")
    await room_send_emote(room.room_id, "Problem with local database, check app logs.")
    await mmm_common.matrix_client.room_leave(room.room_id)
    await mmm_common.matrix_client.room_forget(room.room_id)
    return None
  elif( rooms_db_id == 0 ):
    mmm_log.warning("mmm_matrix.m_room_message_callback", "Can't Find Local room. Leaving.")
    await room_send_emote(room.room_id, "Cant find room in Database. Make a new room and invite me.")
    await mmm_common.matrix_client.room_leave(room.room_id)
    await mmm_common.matrix_client.room_forget(room.room_id)
    return None
  else:
    mmm_log.info("mmm_matrix.m_room_message_callback", "Good. found room.")
    mmm_log.debug("mmm_matrix.m_room_message_callback", "room id: " + str(rooms_db_id) )


  #We have a room, Build up the messsage pack to write to DB.
  message_pack = copy.deepcopy(mmm_common.message_pack)

  message_pack.update({ "rooms_id": rooms_db_id })


  #Get the numbers
  list_of_cell_numbers = mmm_database.matrix_room_to_numbers(room.room_id)

  if( list_of_cell_numbers == None ):
    mmm_log.critical("mmm_matrix.m_room_message_callback", "Problem with Database.")
    await room_send_emote(room.room_id, "Problem with local database, check app logs.")
    await mmm_common.matrix_client.room_leave(room.room_id)
    await mmm_common.matrix_client.room_forget(room.room_id)
    return None

  elif( len(list_of_cell_numbers) == 0 ):
    mmm_log.warning("mmm_matrix.m_room_message_callback", "Can't Find cell numbers for this room. Leaving.")
    await room_send_emote(room.room_id, "We have a room without cellphone numbers. Check App logs.")
    await mmm_common.matrix_client.room_leave(room.room_id)
    await mmm_common.matrix_client.room_forget(room.room_id)
    return None

  elif( len(list_of_cell_numbers) == 1 ):
    mmm_log.info("mmm_matrix.m_room_message_callback", "found 1:1 message.")
    message_pack.update({"number": list_of_cell_numbers[0]})

  elif( len(list_of_cell_numbers) > 1 ):
    mmm_log.info("mmm_matrix.m_room_message_callback", "found group message.")

    #get the name of the group from our database.
    mmsgroup = mmm_database.find_mms_group(list_of_cell_numbers)

    if( mmsgroup == None ):
      mmm_log.critical("mmm_matrix.m_room_message_callback", "Problem With Database")
      await room_send_emote(room.room_id, "Problem with database while getting local group name. check the logs.")
      await mmm_common.matrix_client.room_leave(room.room_id)
      await mmm_common.matrix_client.room_forget(room.room_id)
      return None

    elif( mmsgroup == 0 ):
      mmm_log.error("mmm_matrix.m_room_message_callback", "No group found")
      await room_send_emote(room.room_id, "No local group found for this room. Check logs")
      await mmm_common.matrix_client.room_leave(room.room_id)
      await mmm_common.matrix_client.room_forget(room.room_id)
      return None

    if( mmsgroup.startswith("group") ):
      mmm_log.info("mmm_matrix.m_room_message_callback", "found group message.")
      mmm_log.debug("mmm_matrix.m_room_message_callback", "Group in db: " + str(mmsgroup) )
      message_pack.update({"mmsgroup": mmsgroup })

    else:
      mmm_log.error("mmm_matrix.m_room_message_callback", "Group isn't a group. Check the database")
      await room_send_emote(room.room_id, "mms group found but it's not right. Check logs.")
      await mmm_common.matrix_client.room_leave(room.room_id)
      await mmm_common.matrix_client.room_forget(room.room_id)
      return None

  else:
    mmm_log.critical("mmm_matrix.m_room_message_callback", "Something Weird happened. check list_of_cell_numbers for this room.")
    await room_send_emote(room.room_id, "Problem pulling Numbers from database. Please check logs.")
    await mmm_common.matrix_client.room_leave(room.room_id)
    await mmm_common.matrix_client.room_forget(room.room_id)
    return None
    return None




  try:
    matrix_message_type = event.source['content']['msgtype']
  except Exception as problem:
    mmm_log.error("mmm_matrix.m_room_message_callback", "No message type for matrix message.")
    mmm_log.debug("mmm_matrix.m_room_message_callback", "Exception: " + str(problem))

  if( matrix_message_type == "m.text" ):
    mmm_log.info("mmm_matrix.m_room_message_callback", "m.text message from matrix.")
    try:
      message_pack.update({ "state": mmm_common.MMSmsState.MM_SMS_STATE_SENDING.value })
      message_pack.update({ "text": event.source['content']['body'] })
      message_pack.update({ "size": len(event.source['content']['body']) })
      message_pack.update({ "timestamp": event.source['origin_server_ts'] })
      message_pack.update({ "event": event.source['event_id'] })
      message_pack.update({ "content_type": "text/plain" })
    except Exception as problem:
      mmm_log.error("mmm_matrix.m_room_message_callback", "Problem pulling data from file")
      mmm_log.debug("mmm_matrix.m_room_message_callback", "Problem: " + str(problem))
      return None

  else:
    mmm_log.info("mmm_matrix.m_room_message_callback", "m.file message from matrix")
    try:
      message_pack.update({ "state": mmm_common.MMSmsState.MM_SMS_STATE_SENDING.value })
      message_pack.update({ "text": str(event.body) })
      message_pack.update({ "data": str(event.source['content']['url']) })
      message_pack.update({ "timestamp": str(event.server_timestamp) })
      message_pack.update({ "content_type": str(event.source['content']['info']['mimetype']) })
      message_pack.update({ "size": int(event.source['content']['info']['size']) })
      message_pack.update({ "event": str(event.event_id) })
    except Exception as problem:
      mmm_log.error("mmm_matrix.m_room_message_callback", "Problem pulling data from file")
      mmm_log.debug("mmm_matrix.m_room_message_callback", "Problem: " + str(problem))
      return None


  #Now that we've built up the message pack, Write it to the Databse.
  mmm_log.info("mmm_matrix.m_room_message_callback", "trying to write to database")
  mmm_log.debug("mmm_matrix.m_room_message_callback", "Message pack for DB:" + str(message_pack))
  db_message_id = mmm_database.write_new_message(message_pack)

  sent_status = None

  if( db_message_id == None ):
    await room_send_emote(room.room_id, "Message not sent due to problem with database. Check the logs")

  elif( db_message_id > 0 ):
    mmm_log.info("mmm_matrix.m_room_message_callback", "Message written to Database.")
    await move_receipt_marker(room.room_id, message_pack['event'])




    bridged_status = await matrix_to_cell(list_of_cell_numbers, message_pack)

    if( bridged_status != None ):
      mmm_log.info("mmm_matrix.m_room_message_callback", "Bridged!")
      
      mmm_database.mark_message_as_bridged(db_message_id)
      #if():
      #  marked
      #else:
      #  failed to mark
      sent_status = 0

    else:
      mmm_log.info("mmm_matrix.m_room_message_callback", "Failed to bridge.")

  else:
    await room_send_emote(room.room_id, "Message not sent. Insert to database failed. Check the logs")
    mmm_log.error("mmm_matrix.m_room_message_callback", "Failed to write to Database.")

  return sent_status

async def matrix_to_cell(list_of_cell_numbers, message_pack):
  """Goal: Take a message pack and a list of numbers and send it to the
  correct modem.

  SMS -> MM
  MMS -> MMSD

  return None - Failed clean
  return 0    - Everything Perfect.
  return 1    - SMS Sent but stuck in MM (bad)
  return 2    - MMS sent but stuck in mmsd (bad)
  return 3    - SMS NOT sent AND stuck on modem (bad)
  """
  mmm_log.info("mmm_matrix.matrix_to_cell", "Called")

  mms_message = False
  send_status = None


  if( message_pack['content_type'] == "text/plain" and len(list_of_cell_numbers) == 1 ):
    mmm_log.info("mmm_matrix.matrix_to_cell", "Text message to one person. SMS")
    mms_message = False

  elif( message_pack['content_type'] == "text/plain" and len(list_of_cell_numbers) > 1 ):
    mmm_log.info("mmm_matrix.matrix_to_cell", "Text message group. MMS")
    mms_message = True

  else:
    mmm_log.debug("mmm_matrix.matrix_to_cell", "mms size: " + str(message_pack[ 'size' ]) + " max size: " + str(mmm_common.max_mms_size) )

    http_url = await mxc_to_url(message_pack[ 'data' ])
    if( http_url == None ):
      mmm_log.error("mmm_matrix.matrix_to_cell", "failed to get url from mxc")
      return None
    else:
      mmm_log.debug("mmm_matrix.matrix_to_cell", "URL: " + http_url )

    if( message_pack[ 'size' ] <= mmm_common.max_mms_size ):
      mmm_log.info("mmm_matrix.matrix_to_cell", "Trying to download file")

      file_path = await download_http_file(http_url, message_pack[ 'text' ])
      if( file_path == None ):
        mmm_log.warning("mmm_matrix.matrix_to_cell", "failed to download file. Send URL insead, why not?")
        message_pack.update({"text": http_url})
        message_pack.update({"data": ""})
        if ( len(list_of_cell_numbers) == 1 ):
          mms_message = False
        else:
          mms_message = True

      else:
        mmm_log.info("mmm_matrix.matrix_to_cell", "File Downloaded")
        mmm_log.debug("mmm_matrix.matrix_to_cell", "File path: " + file_path )
        message_pack.update({"text": ""})
        message_pack.update({"data": file_path})
        mms_message = True
    else:
      mmm_log.info("mmm_matrix.matrix_to_cell", "File too big, Send URL.")
      message_pack.update({"text": http_url})
      message_pack.update({"data": ""})
      if ( len(list_of_cell_numbers) == 1 ):
        mms_message = False
      else:
        mms_message = True




  mmm_log.info("mmm_matrix.matrix_to_cell", "message fixed up for sending")
  mmm_log.debug("mmm_matrix.matrix_to_cell", "Send MMS: " + str(mms_message) + " - message pack: " + str(message_pack) )




  if(mms_message):
   mmm_log.info("mmm_matrix.matrix_to_cell", "MMS to send")

   sent_mmsd_dbus_url = mmm_mmsd.send_mms(list_of_cell_numbers, message_pack['text'], message_pack['data'], message_pack['content_type'])
   if( sent_mmsd_dbus_url == None ):
     mmm_log.warning("mmm_matrix.matrix_to_cell", "Failed to send MMS")
     return None
   else:
     mmm_log.warning("mmm_matrix.matrix_to_cell", "Sent MMS!")
     send_status = 0

     #TODO - Where to delete off mmsd?
     #delete_sent_mmsd = mmm_mmsd.delete_mms(sent_mmsd_dbus_url)
     #if( delete_sent_mmsd == None):
     #  mmm_log.critical("mmm_matrix.matrix_to_cell", "Sent but stuck in mmsd.")
     #  send_status = 2

  else:
   mmm_log.info("mmm_matrix.matrix_to_cell", "SMS to send")
   active_modem = mmm_mm.get_active_modem()
   if( active_modem == None ):
     mmm_log.warning("mmm_matrix.matrix_to_cell", "Modem Not found.")
     return None

   mmm_log.info("mmm_matrix.matrix_to_cell", "Create SMS on Modem")
   dbus_sms_url = mmm_mm.sms_create(active_modem, message_pack['number'], message_pack['text'])
   if( dbus_sms_url == None):
     mmm_log.error("mmm_matrix.matrix_to_cell", "Can't create on MM")
     return None

   mmm_log.info("mmm_matrix.matrix_to_cell", "Sending SMS")
   sms_send_return = mmm_mm.sms_send(dbus_sms_url)
   if( sms_send_return == None):
     mmm_log.warning("mmm_matrix.matrix_to_cell", "Failed Sending SMS")
     send_status == None
   else:
     mmm_log.info("mmm_matrix.matrix_to_cell", "SMS Sent.")
     send_status = 0

   mmm_log.info("mmm_matrix.matrix_to_cell", "Trying to Delete the SMS from the modem.")
   sms_delete_return = mmm_mm.delete_sms(active_modem, dbus_sms_url)
   if( sms_delete_return == None and send_status == None):
     mmm_log.critical("mmm_matrix.matrix_to_cell", "Not Sent AND stuck in MM.")
     send_status = 3

   elif( sms_delete_return == None and send_status == 0):
     mmm_log.critical("mmm_matrix.matrix_to_cell", "Sent but stuck in MM.")
     send_status = 1

  return send_status

async def upload_file(matrix_client, message):
  """Goal: Upload file to matrix.

  return None               - Failure
  return.startswith("mxc:") - success
  """#NOTE to self = Good
  mmm_log.info("mmm_matrix.upload_file", "Called.")

  mxc_url = None
  
  try:
    async with aiofiles.open(message['data'], "r+b") as open_file:
      resp, maybe_keys = await matrix_client.upload(
          open_file,
          content_type=message['content_type'], 
          filename=message['data'],
          filesize=message['size'])
  except Exception as problem:
    mmm_log.error("mmm_matrix.upload_file", "Can't upload file!")
    mmm_log.debug("mmm_matrix.upload_file", "Exception " + str(problem) )
    return None

  try:
    if( resp.content_uri.startswith( "mxc:" ) ):
      mxc_url = str(resp.content_uri)
  except Exception as problem:
    mmm_log.error("mmm_matrix.upload_file", "not an mxc url???")
    mmm_log.debug("mmm_matrix.upload_file", "Exception " + str(problem) )


  return mxc_url

async def room_send_m_message(matrix_client, matrix_room, content ):
  """
  """
  mmm_log.info("mmm_matrix.room_send_m_message", "Called.")

  resp = None

  try:
    resp = await matrix_client.room_send( room_id      = matrix_room,
                                          message_type = "m.room.message",
                                          content      = content
                                        )
  except Exception as problem:
    mmm_log.critical("mmm_matrix.room_send_m_message", "failed to write message to matrix room")
    mmm_log.debug("mmm_matrix.room_send_m_message", "Exception " + str(problem) )
    return None


  try:
    event_id = str(resp.event_id)
  except Exception as problem:
    mmm_log.critical("mmm_matrix.room_send_m_message", "problem with event_id")
    mmm_log.debug("mmm_matrix.room_send_m_message", "Exception " + str(problem) )


  if( event_id != None and event_id.startswith( '$' )):
    mmm_log.info("mmm_matrix.room_send_m_message", "Bridged!")
    mmm_log.debug("mmm_matrix.room_send_m_message", "Bridged: " + event_id )
    return event_id

  else:
    mmm_log.error("mmm_matrix.room_send_m_message", "Event ID is weird.")
    mmm_log.debug("mmm_matrix.room_send_m_message", "Problem: doesn't start with $ ---> " + event_id)
    return None

async def bridge_message_to_room(message):
  """This will take a message pack (SMS or MMS) and bridge it to a
  matrix room. If there is none, it will make one.
  """
  mmm_log.info("mmm_matrix.bridge_message_to_room", "Called.")
  mmm_log.debug("mmm_matrix.bridge_message_to_room", "db_message_id: " + str(message['message_id']) + " - message: " + str(message))

  matrix_send_return = None

  #If it's an SMS WAP, its actually an MMS and we need to tell mmsd.
  if( message['content_type'] == "sms/wap" ):
    mmm_log.critical("mmm_matrix.bridge_message_to_room", "You got here by mistake, send me an issue with the log")
    return None

  #If it's an SMS WAP, its actually an MMS and we need to tell mmsd.
  if( message['message_id'] == None or message['message_id'] == 0 ):
    mmm_log.critical("mmm_matrix.bridge_message_to_room", "We should never get here. The message NEEDS to be in the database before we get here.")
    return None


  #Check the database for a room
  matrix_room = mmm_database.find_matrix_room_id(message['number'], message['mmsgroup'] )

  if( matrix_room == None ):
    mmm_log.critical("mmm_matrix.bridge_message_to_room", "Problem talking to Database. retry later.")
    return None
  elif( matrix_room == 1 ):
    mmm_log.critical("mmm_matrix.bridge_message_to_room", "MM/MMSD didn't make a local room! retry later.")
    return None
  elif( matrix_room == 0 ):
    mmm_log.info("mmm_matrix.bridge_message_to_room", "Found local room with no matrix room reference. Need to make one.")


    #make room
    new_matrix_room = None
    if( message['mmsgroup'].startswith("group:") ):
      mmm_log.info("mmm_matrix.bridge_message_to_room", "Make a Matrix room for a group chat")
      try:
        #TODO get list of group numbers, add it to topic.
        number_list = mmm_database.local_group_to_number_list(message['mmsgroup'])
        new_matrix_room = await make_room(name=message['mmsgroup'], topic=str(number_list))
      except Exception as problem:
        print(f"aaaaaaaaaaaa {problem} ")
    else:
      mmm_log.info("mmm_matrix.bridge_message_to_room", "Make a Matrix room for a 1:1 chat")
      #TODO - Contact name
      new_matrix_room = await make_room(direct=True, name=message['number'], topic=message['number'])

    #If we have a Matirx room, Match it with the local room and update database.
    if( new_matrix_room.room_id != None and new_matrix_room.room_id.startswith( '!' ) ):
      mmm_log.info("mmm_matrix.bridge_message_to_room", "Made a room, Add it to the Database.")
  
      if( message['mmsgroup'].startswith("group:") ):
        db_room_id = mmm_database.find_local_room_id(message['mmsgroup'])
      else:
        db_room_id = mmm_database.find_local_room_id(message['number'])

      if( db_room_id > 0 ):
        mmm_log.info("mmm_matrix.bridge_message_to_room", "Matched room without matrix ID. Adding it")
        mmm_database.add_matirx_room_to_local_room(db_room_id, new_matrix_room.room_id )
        matrix_room = new_matrix_room.room_id

    else:
      mmm_log.error("mmm_matrix.bridge_message_to_room", "Failed to create Matrix room.")
      return None


  if( matrix_room.startswith("!") ):
    mmm_log.info("mmm_matrix.bridge_message_to_room", "After all that we have a room.")
    mmm_log.debug("mmm_matrix.bridge_message_to_room", "room: " + str(matrix_room))
  else:
    mmm_log.error("mmm_matrix.bridge_message_to_room", "Something happened. We dont have a room.")
    return None


  mmm_log.debug("mmm_matrix.bridge_message_to_room", "Sending database message ID: " + str(message['message_id']) + " to matrix")


  #if its a file, upload the file
  if( message['data'].startswith( "/" ) ):
    mmm_log.debug("mmm_matrix.bridge_message_to_room", "need to upload: " + str(message['data']) + " to matrix")


    mxc_url = await upload_file(mmm_common.matrix_client, message)

    if( mxc_url == None ):
      mmm_log.error("mmm_matrix.bridge_message_to_room", "problem uploading file")
      return None
    else:
      mmm_log.info("mmm_matrix.bridge_message_to_room", "File uploaded.")
      mmm_log.debug("mmm_matrix.bridge_message_to_room", "mxc url: " + mxc_url)

      content = { "body": message['content_type'] + " File from " + message['number'], 
                  "info": { "size": message['size'],
                            "mimetype": message['content_type'],
                          },
                  "msgtype": "m.image",
                  "url": mxc_url,
                  "puppet_info": { "rooms_id":        message['rooms_id'],
                                   "message_id":      message['message_id'],
                                   "data":            message['data'],
                                   "text":            message['text'],
                                   "state":           message['state'],
                                   "number":          message['number'],
                                   "mmsgroup":        message['mmsgroup'],
                                   "timestamp":       message['timestamp'],
                                   "content_type":    message['content_type'],
                                   "size":            message['size'],
                                   "bridged_attempt": message['bridged_attempt'],
                                   "event":           message['event'],
                                 },
                }

      matrix_send_return = await room_send_m_message(mmm_common.matrix_client, matrix_room, content )

  #otherwise send the text.
  else:
    mmm_log.info("mmm_matrix.bridge_message_to_room", "No file to upload. normal text.")
    
    #prepend phone number to group messages.
    #TODO contact
    if( message['mmsgroup'].startswith("group:") ):
      msg_body = message['number'] + ": " + message['text']
    else:
      msg_body = message['text']



    content = { "body": msg_body, 
                "msgtype": "m.text",
                "puppet_info": { "rooms_id":        message['rooms_id'],
                                 "message_id":      message['message_id'],
                                 "state":           message['state'],
                                 "number":          message['number'],
                                 "mmsgroup":        message['mmsgroup'],
                                 "timestamp":       message['timestamp'],
                                 "content_type":    message['content_type'],
                                 "size":            message['size'],
                                 "bridged_attempt": message['bridged_attempt'],
                                 "event":           message['event'],
                               },
              }

    matrix_send_return = await room_send_m_message(mmm_common.matrix_client, matrix_room, content )

  if( matrix_send_return != None ):
    mmm_log.info("mmm_matrix.bridge_message_to_room", "bridge successful. Need to tell database.")
    mmm_log.debug("mmm_matrix.bridge_message_to_room", "bridge successful. bridged event_id: " + matrix_send_return)
    
    update_database_return = mmm_database.mark_message_as_bridged(message['message_id'])

    if( update_database_return != None and update_database_return > 0 ):
      mmm_log.info("mmm_matrix.bridge_message_to_room", "Marked as bridged in the database!")
    else:
      mmm_log.warning("mmm_matrix.bridge_message_to_room", "Failed to mark as bridged. this will be a duplicate at some point")
  else:
    mmm_log.error("mmm_matrix.bridge_message_to_room", "Failed to bridge event.")


  return 0

async def move_receipt_marker(room_id, event_id):
  """Move the read receipt marker to event_id

  Returns 0 success
  Returns None on fail.
  """
  mmm_log.info("mmm_matrix.move_receipt_marker", "Called.")

  try:
    await mmm_common.matrix_client.update_receipt_marker(room_id, event_id, receipt_type='m.read') 
  except Exception as problem:
    mmm_log.error("mmm_matrix.move_receipt_marker", "Error moving read marker")
    mmm_log.debug("mmm_matrix.move_receipt_marker", problem)
    return None

  return 0

async def get_room_name(room_id):
  mmm_log.info("mmm_matrix.get_room_name", "Called" )

  room_name = None

  try:
    room_name_event = await mmm_common.matrix_client.room_get_state_event(room_id, event_type = "m.room.name" )
    room_name = str(room_name_event.content['name'])

  except Exception as problem:
    mmm_log.error("mmm_matrix.get_room_name", "Problem Getting Room name." )
    mmm_log.debug("mmm_matrix.get_room_name", "Problem Getting Room name: " + str(problem) )

  return room_name

async def get_room_topic(room_id):
  mmm_log.info("mmm_matrix.get_room_topic", "Called" )

  room_topic = None

  try:
    room_topic_event = await mmm_common.matrix_client.room_get_state_event(room_id, event_type = "m.room.topic" )
    room_topic = str(room_topic_event.content['topic'])

  except Exception as problem:
    mmm_log.error("mmm_matrix.get_room_topic", "Problem Getting Room Topic." )
    mmm_log.debug("mmm_matrix.get_room_topic", "Problem Getting Room Topic: " + str(problem) )

  return room_topic

async def make_room(direct=False, name="Empty", topic="Empty"):
  """Goal, Make a room for us to bridge messages to.
  Direct chats are for 1:1
      - Name = Contact name or phone number.
      - topic = Phone number
  Non Direct are group.
      - Name - groupId (you can change this)
      - Topic, csv of numbers

  return None            - problem
  return.startswith("!") - we have a room
  """#Note to self = Good
  mmm_log.info("mmm_matrix.make_room", "Called" )

  new_matrix_room = None

  try:
    new_matrix_room = await mmm_common.matrix_client.room_create(visibility=RoomVisibility.private, name=name, topic=topic, federate=False, is_direct=direct, invite=(mmm_common.matrix_users_list))
  except Exception as problem:
    mmm_log.critical("mmm_matrix.make_room", problem)




  return new_matrix_room





  
