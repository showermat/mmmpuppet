#!/usr/bin/python3


##Logging##
import logging
import logging.handlers as handlers
import threading

import mmm_common


#TODO - I dont know how to not put this here. I also do it in mmmpuppet.py main()
from pathlib import Path
Path(mmm_common.BASE_DIR).mkdir(parents=True, exist_ok=True)
#TODO - I dont know how to not put this here. I also do it in mmmpuppet.py main()


##Logging##
LOG_LEVEL = 10
LOG_SIZE_BYTES = 10000000
LOG_BACKUP_COUNT = 2


LOG_LOCK = threading.Lock()
logger = logging.getLogger('MMMPuppet')
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

log_handler = handlers.RotatingFileHandler(mmm_common.LOG_FILE, maxBytes=LOG_SIZE_BYTES, backupCount=LOG_BACKUP_COUNT)
log_handler.setLevel(LOG_LEVEL)
log_handler.setFormatter(formatter)

logger.addHandler(log_handler)


def critical(function, message):
  with LOG_LOCK:
    logger.critical(f"[{function}] - {message}")

def error(function, message):
  with LOG_LOCK:
    logger.error(f"[{function}] - {message}")

def warning(function, message):
  with LOG_LOCK:
    logger.warning(f"[{function}] - {message}")

def info(function, message):
  with LOG_LOCK:
    logger.info(f"[{function}] - {message}")

def debug(function, message):
  with LOG_LOCK:
    logger.debug(f"[{function}] - {message}")
