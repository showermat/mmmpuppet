#!/usr/bin/python3

import mmm_common
import mmm_log
import mmm_database
import mmm_mmsd
import mmm_matrix

from gi.repository import GLib
import dbus
import dbus.mainloop.glib

import asyncio

import time

import copy


def get_active_modem():
  """Goal: Find and return the first active modem DBUS path.
  IE: /org/freedesktop/ModemManager1/Modem/0

  I've noticed the modem will change if the phone goes to sleep so this
  will need to be called before interacting with the modem each time.
  There's probably a better way to do this but I cant figure it out.

  Success - return String of modem
  Fail    - return None
  """#TODO - I bet there's a better way to do this.
  mmm_log.info("mmm_mm.get_active_modem", "Called.")

  modem_objects = None
  active_modem = None

  #build the dbus connection to search for modem
  system_bus = dbus.SystemBus()
  mm_modem = system_bus.get_object('org.freedesktop.ModemManager1', '/org/freedesktop/ModemManager1')
  bus_iface = dbus.Interface(mm_modem, dbus_interface='org.freedesktop.DBus.ObjectManager')

  #Try and actually Search for the modem path on the dbus.
  try:
    modem_objects = bus_iface.GetManagedObjects('/org/freedesktop/ModemManager1/Modem')

  except Exception as problem:
    mmm_log.critical("mmm_mm.get_active_modem", "Cant find an active modem.")
    mmm_log.debug("mmm_mm.get_active_modem", "Exception: " + str(problem))


  #pull out the dbus path of the modem, If we have one.
  if(modem_objects != None):
    for modem in modem_objects:
      active_modem = modem
      break

  return active_modem

def get_sms_data_from_mm(dbus_sms_url):
  """
  """
  system_bus = dbus.SystemBus()
  dbus_sms_object = system_bus.get_object('org.freedesktop.ModemManager1', dbus_sms_url)
  dbus_sms_properties = dbus.Interface(dbus_sms_object, dbus_interface='org.freedesktop.DBus.Properties')
  sms_properties = dbus_sms_properties.GetAll("org.freedesktop.ModemManager1.Sms")


  sms_data_dict = copy.deepcopy(mmm_common.message_pack)
  sms_data_dict.update({"SmsDbusPath":str(dbus_sms_url)})
  sms_byte_data = []
  
  #We're going to make the number a list of one element. If it turns
  #out to be a group text we'll be ready.

  for key in sms_properties:
    if( key == "State" ):
      sms_data_dict.update({"state":int(sms_properties[key])})
    elif( key == "Number" ):
      formatted_incoming_number = mmm_common.format_number(str(sms_properties[key]))
      sms_data_dict.update({"number": formatted_incoming_number})
    elif( key == "PduType" ):
      continue
    elif( key == "Text" ):
      sms_data_dict.update({"text":str(sms_properties[key])}) 
    elif( key == "Data" ):
      for byte in sms_properties[key]:
        sms_byte_data.append(byte)
      sms_data_dict.update( { "data" : sms_byte_data } )
    elif( key == "SMSC" ):
      continue
    elif( key == "Validity" ):
      continue
    elif( key == "Class" ):
      continue
    elif( key == "TeleserviceId" ):
      continue
    elif( key == "ServiceCategory" ):
      sms_data_dict.update({"service_category":str(sms_properties[key])})
    elif( key == "DeliveryReportRequest" ):
      sms_data_dict.update({"delivery_report_request":str(sms_properties[key])})
    elif( key == "MessageReference" ):
      continue
    elif( key == "Timestamp" ):
      sms_data_dict.update({"timestamp":str(sms_properties[key])}) 
    elif( key == "DischargeTimestamp" ):
      continue
    elif( key == "DeliveryState" ):
      sms_data_dict.update({"delivery_state":str(sms_properties[key])})
    elif( key == "Storage" ):
      continue
    else:
      print(key, ": ", sms_properties[key], ". Not implemented.")

  return sms_data_dict

def sms_create(active_modem, number, text, delivery_report_request=True):
  """Goal: Create an SMS message on the Modem. This is required before
  we can do anything with it. 

  Success - return string of dbus path of new message on modem.
  Fail    - return None
  """#Note for myself = Good
  mmm_log.info("mmm_mm.sms_create", "Called.")
  dbus_sms_url = None


  #Build up the DBUS messaging interface to create a new SMS
  system_bus = dbus.SystemBus()
  dbus_modem_object = system_bus.get_object('org.freedesktop.ModemManager1', active_modem)
  dbus_messaging_interface = dbus.Interface(dbus_modem_object, dbus_interface='org.freedesktop.ModemManager1.Modem.Messaging')

  #build the data pack to send across dbus to MM
  dbus_sms_package = dbus.Dictionary( {"number" : number, "text" : text, "delivery-report-request": delivery_report_request }, signature='sv')

  #Send the data pack to MM and get the ID.
  try:
    dbus_sms_url = dbus_messaging_interface.Create(dbus_sms_package)
  except Exception as problem:
    mmm_log.critical("mmm_mm.sms_create", "Problem Creating message on Modem.")
    mmm_log.debug("mmm_mm.sms_create", "Exception: " + str(problem))
    
  return dbus_sms_url

def sms_send(dbus_sms_url):
  """Goal: Take a dbus path of a created SMS message and attempt to send
  it via Modem Manager.

  Success - return 0
  Fail    - return None
  """#Note for myself = Good
  mmm_log.info("mmm_mm.sms_send", "Called.")

  send_sms_return = None
  
  #Now that we have the new sms created and its id attach to it
  system_bus = dbus.SystemBus()
  dbus_sms_object = system_bus.get_object('org.freedesktop.ModemManager1', dbus_sms_url)
  dbus_sms_interface = dbus.Interface(dbus_sms_object, dbus_interface='org.freedesktop.ModemManager1.Sms')

  try:
    #the MM to send the new sms we created.
    send_sms_return = dbus_sms_interface.Send() #This always returns none. I guess if the try doesn't fail we're good.
    send_sms_return = 0

  except Exception as problem:
    mmm_log.critical("mmm_mm.sms_send", "Problem sending SMS")
    mmm_log.debug("mmm_mm.sms_send", "Exception: " + str(problem))

  return send_sms_return

def delete_sms(active_modem, dbus_sms_url):
  """Goal: Delete an SMS message off the Modem.

  Success - return 0
  Fail    - return None
  """#Note for myself = Good
  mmm_log.info("mmm_mm.delete_sms", "Called.")
  delete_sms_return = None

  #Build up the messaging interface to delete an sms
  system_bus = dbus.SystemBus()
  dbus_modem_object = system_bus.get_object('org.freedesktop.ModemManager1', active_modem)
  dbus_messaging_interface = dbus.Interface(dbus_modem_object, dbus_interface='org.freedesktop.ModemManager1.Modem.Messaging')

  try:
    #using the path delete the sms.
    delete_sms_return = dbus_messaging_interface.Delete(dbus_sms_url) #This always returns none. I guess if the try doesn't fail we're good.
    delete_sms_return = 0

  except Exception as problem:
    mmm_log.critical("mmm_mm.delete_sms", "Cant delete message off modem")
    mmm_log.debug("mmm_mm.delete_sms", "Exception: " + str(problem))

  return delete_sms_return

def message_added(dbus_sms_path, mm_received):
  """Goal: Take message from Modem and notify the correct party.

  SMS path: These are simple messages so try to create a room in the DB
            Then write the message into the message table. If both these
            things work then we will remove it from the Modem and send
            to bridge.

  MMS Path: This is more complicated. The only information we can trust
            in the modem is the 'data' information. this has the url
            of the .mms file to be downloaded. The rest, such as the
            phone number, isn't real (for me). We take the 'data' from
            the SMS WAP and send it over the dbus to MMSD. This service
            keeps track of its own messages so if this doesn't error
            we delete the message from the modem. After this point We'll
            need to hit the MMSD api over the dbus to get what we need.

  This is called when MM lets us know a new message is on the modem.
  
  dbus_sms_path == the dbus path to the SMS message - /org/freedesktop/ModemManager1/SMS/##
  mm_received   == True if received, Otherwise False.
  """#Note for myself = Good.
  mmm_log.info("mmm_mm.message_added", "Called.")
  mmm_log.debug("mmm_mm.message_added", "sms dbus path: " + str(dbus_sms_path))

  if( not mm_received ):
    mmm_log.info("mmm_mm.message_added", "Outbound. Ignore.")
    return 0

  elif( mm_received ):


    #pull the sms information of the new message
    sms_info = get_sms_data_from_mm(dbus_sms_path)


    #Some times it takes the modem a couple seconds to get the SMS WAP
    # information so we'll wait up to 10 seconds and keep checking.
    wait_count = 0
    while (wait_count <= 5 and sms_info['state'] != 3):
      time.sleep(2)
      mmm_log.info("mmm_mm.message_added", "Checking again to see if message is ready." )
      sms_info = get_sms_data_from_mm(dbus_sms_path)
      wait_count += 1



    if( sms_info['state'] != 3 ):
      mmm_log.warning("mmm_mm.message_added", "We waited 10 seconds and the SMS wasn't ready. Leaving message on modem to retry later." )
      return 0

    elif( len(sms_info['data']) == 0 and sms_info['text'] == ""):
      mmm_log.warning("mmm_mm.message_added", "Empty. Leaving message on modem to retry later.")
      return 0


    #if the SMS message has anything in 'data' then its an MMS waiting
    # to be downloaded. Tell MMSD.
    if( len(sms_info['data']) > 0 ):
      mmm_log.info("mmm_mm.message_added", "Found SMS WAP (MMS) message. Telling mmsd.")

      mmsd_notify_return = mmm_mmsd.notify_sms_wap(sms_info['data'])
      if( mmsd_notify_return != None ):
        mmm_log.info("mmm_mm.message_added", "MMSD has sms data, Deleting sms from modem as its not my problem anymore. Talk to mmsd services for more.")
        active_modem = get_active_modem()
        delete_sms(active_modem, dbus_sms_path)
      else:
        mmm_log.warning("mmm_mm.message_added", "Failed to notify MMSD. Leaving message on modem to retry later.")
        return None

      return 0

    #SMS message does not have 'data' so its a normal message. Write to
    # database and bridge.
    else:
      mmm_log.info("mmm_mm.message_added", "Normal SMS message.")
      
      #Can't write an empty list to the database, Make it a string.
      sms_info.update({"data":""})
      db_message_id = 0

      #Add missing elements to message pack.
      sms_info.update({"size":len(sms_info['text'])})
      sms_info.update({"event":str(dbus_sms_path)})
      sms_info.update({"content_type":"text/plain"})

      mmm_log.info("mmm_mm.message_added", "Looking for local room.")
      db_rooms_id = mmm_database.find_local_room_id(member=sms_info['number'])
      if( db_rooms_id == None ):
        mmm_log.warning("mmm_mm.message_added", "Problem with Database. Leaving message on modem to retry later.")
        return 0
  
      elif( db_rooms_id == 0 ):
        mmm_log.info("mmm_mm.message_added", "No local room, creating one.")
        db_rooms_id = mmm_database.create_local_room_id(member=sms_info['number'])

        if( db_rooms_id == None ):
          mmm_log.warning("mmm_mm.message_added", "Creating loacal room failed.")



      mmm_log.debug("mmm_mm.message_added", "Local room: " + str(db_rooms_id) )
      sms_info.update({"rooms_id":db_rooms_id})

      #write to database
      db_message_id = mmm_database.write_new_message(sms_info)
    



      #If it was written to the db successfully
      if( db_message_id > 0 ):
        sms_info.update({"message_id": db_message_id})
        mmm_log.info("mmm_mm.message_added", "Written to DB successfully")
        mmm_log.debug("mmm_mm.message_added", str(dbus_sms_path) + " is DB id "+ str(db_message_id) + ". Going to delete from modem.")


        #delete message off modem, it's in the database with everything we need.
        active_modem = get_active_modem()
        delete_sms(active_modem, dbus_sms_path)
      
        #send to bridge, not our problem now that its in the Database and not on the modem.
        mmm_log.info("mmm_mm.message_added", "Sending to be bridged. Not my problem any more.")
        asyncio.run_coroutine_threadsafe(mmm_matrix.bridge_message_to_room(sms_info), mmm_common.matrix_event_loop)

      else:
        mmm_log.error("mmm_mm.message_added", "Failed to write to DB. Leaving message on modem to retry later.")
        return 0


    return 0



  else:
    mmm_log.error("mmm_mm.message_added", "You should never get here!")
    mmm_log.debug("mmm_mm.message_added", "sms type unknown: " + sms_type + " - Modem path: " + dbus_sms_path)
    return None

  mmm_log.error("mmm_mm.message_added", "How'd you get here pt.2?!")
  return None
